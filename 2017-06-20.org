#+title: Notes Soirée Emacs
#+date: 2017-06-20

* Présents

- Alexis
- Bastien
- Nicolas
- Steven
- Thierry
- Vivien
- Édouard

* Quelques outils mentionnés lors du parcous des .emacs.el

- global-highlight-changes-mode
- uniquify-buffer-name-style
- ediff-setup-windows-plain
- ido-auto-merge-delay-time
- winpoint: Remember buffer positions per-window, not per buffer
- save-desktop
- mark-ring pour naviguer en parcourant les marks
- helm-swoop: Efficiently hopping squeezed lines powered by helm interface
- https://tmate.io pour partager des terminaux via une URL
- org-capture-template-contexts
- dans un agenda org, C-x C-c test.org RET pour sauvegarder

* Git (avec magit)

** git

- git: l'exécutable
- github/framagit: service web autour de git
- gitlab: application web et service
- gogs: application web
- Raccourci natif dans Emacs : C-x v v

